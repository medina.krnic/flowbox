from flask_restful import Resource
import datafiles





class Persons(Resource):
	def get(self, identity):

		#gets person and id 
		person = [person for person in datafiles.persons if person['id'] == str(identity)][0]
		#gets what city the person is from
		city = [x['city'] for x in [city for city in datafiles.cities if city['id'] == str(identity)]][0]
		#adds city to the person dict
		person["city"] = city
		#gets what country the city belongs to
		country = [x['country'] for x in [country for country in datafiles.countries if country['city'] == city]][0]
		#adds country to the person dict
		person["country"] = country
		#gets what birthdate the person has
		birthdate =  [x['birthdate'] for x in [birthdate for birthdate in datafiles.birthdays if birthdate['id'] == str(identity)]][0]
		#adds birthdate to the person dict
		#person["birthdate"] = birthdate
		person["birthdate"] = '-'.join([birthdate[:4], birthdate[4:6], birthdate[6:]])

		return person

