import csv, json 
def getPersons():

	csvFile = open("data/persons.csv")

	fields = ("id", "name")
	reader = csv.DictReader(csvFile, fields)

	data = []

	for row in reader:
		data.append(row)
	return data


def getCities():

	csvFile = open("data/cities.csv")

	fields = ("id", "city")
	reader = csv.DictReader(csvFile, fields)

	data = []

	for row in reader:
		data.append(row)
	return data


def getCountries():

	csvFile = open("data/countries.csv")

	fields = ("city", "country")
	reader = csv.DictReader(csvFile, fields)

	data = []

	for row in reader:
		data.append(row)
	return data

def getBirthdays():

	csvFile = open("data/birthdays.csv")

	fields = ("id", "birthdate")
	reader = csv.DictReader(csvFile, fields)

	data = []

	for row in reader:
		data.append(row)
	return data

persons = getPersons()

cities = getCities()

countries = getCountries()

birthdays = getBirthdays()