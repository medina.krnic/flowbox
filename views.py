from flask import Flask, abort, request, jsonify
from flask_restful import Resource, Api
from datetime import datetime
import csv, json 
import datafiles
import models
from models import Persons

#init app
app = Flask(__name__)
api = Api(app)


persons = datafiles.persons

cities = datafiles.cities

countries = datafiles.countries

birthdays = datafiles.birthdays



#Test endpoint
@app.route('/')
def index():
	return "Hello world!"

#Endpoint to get person by id
api.add_resource(Persons, '/persons/<int:identity>')

#Endpoint to get all people in a city
@app.route('/city/<string:city>',methods=['GET'])
def getCityPeople(city):
	cityPeople =  [cityPeople for cityPeople in cities if cityPeople['city'] == city]
	personIDs = [x['id'] for x in cityPeople]
	person = [(Persons().get(int(x))) for x in personIDs]

	return jsonify(person)


#Endpoint to get all people in a city by page 
@app.route('/city/<string:city>/page=<int:page>',methods=['GET'])
def getCityPage(city, page):
	cityPeople =  [cityPeople for cityPeople in cities if cityPeople['city'] == city]
	personIDs = [x['id'] for x in cityPeople]
	person = [(Persons().get(int(x))) for x in personIDs]
	pageStart = (page - 1) * 25
	pageEnd = page * 25
	person = person[pageStart:pageEnd]

	return jsonify(person)

#Endpoint to get all people in a country
@app.route('/country/<string:country>',methods=['GET'])
def getCountryPeople(country):
	countryCity = [countryCity for countryCity in countries if countryCity['country'] == country]
	cityNames = [x['city'] for x in countryCity]
	cityPeople = [cityPeople for cityPeople in cities if cityPeople['city'] in cityNames]
	personIDs = [x['id'] for x in cityPeople]
	person = [(Persons().get(int(x))) for x in personIDs]


	return jsonify(person)
	

#Endpoint to get all people in a country by page
@app.route('/country/<string:country>/page=<int:page>',methods=['GET'])
def getCountryPage(page, country):
	countryCity = [countryCity for countryCity in countries if countryCity['country'] == country]
	cityNames = [x['city'] for x in countryCity]
	cityPeople = [cityPeople for cityPeople in cities if cityPeople['city'] in cityNames]
	personIDs = [x['id'] for x in cityPeople]
	person = [(Persons().get(int(x))) for x in personIDs]
	pageStart = (page - 1) * 25
	pageEnd = page * 25
	person = person[pageStart:pageEnd]


	return jsonify(person)


#Endpoint to get all people by order of age
@app.route('/people/age=<string:order>',methods=['GET'])
def getPeople(order):
	
	allPeople =  [allPeople for allPeople in persons if allPeople['id'] != 'id']
	personIDs = [x['id'] for x in allPeople]
	person = [(Persons().get(int(x))) for x in personIDs]
	if (order == "descending"):
		person.sort(key = lambda x: datetime.strptime(x['birthdate'], '%Y-%m-%d')) 
	elif (order == "ascending"):
		person.sort(key = lambda x: datetime.strptime(x['birthdate'], '%Y-%m-%d'), reverse = True)
	else:
		person = "You have to enter ascending or descending" 

	return jsonify(person)




#Endpoint to get all people by order of age by page
@app.route('/people/age=<string:order>/page=<int:page>',methods=['GET'])
def getPeoplePage(order, page):
	
	allPeople =  [allPeople for allPeople in persons if allPeople['id'] != 'id']
	personIDs = [x['id'] for x in allPeople]
	person = [(Persons().get(int(x))) for x in personIDs]
	if (order == "descending"):
		person.sort(key = lambda x: datetime.strptime(x['birthdate'], '%Y-%m-%d')) 
	elif (order == "ascending"):
		person.sort(key = lambda x: datetime.strptime(x['birthdate'], '%Y-%m-%d'), reverse = True)
	else:
		person = ["You have to enter ascending or descending"]

	pageStart = (page - 1) * 25
	pageEnd = page * 25
	person = person[pageStart:pageEnd]

	return jsonify(person)



#Endpoint to get all people/person with the next closest birthday to today
@app.route('/birthdays/next',methods=['GET'])
def getNextBirthday():
	allPeople =  [allPeople for allPeople in persons if allPeople['id'] != 'id']
	personIDs = [x['id'] for x in allPeople]
	allPeople = [(Persons().get(int(x))) for x in personIDs]
	allBirthdays = ["2020"+x['birthdate'][4:] for x in allPeople]
	allBirthdays.sort(key = lambda x: datetime.strptime(x, '%Y-%m-%d'))
	today = datetime.today().strftime('%Y-%m-%d')
	nextDate = min([d for d in allBirthdays if d>str(today)], key=lambda s: datetime.strptime(s, "%Y-%m-%d")-datetime.strptime(today, "%Y-%m-%d"))
	nextDate = nextDate[4:]
	p = [p for p in allPeople if nextDate in p['birthdate']]
	return jsonify(p)







